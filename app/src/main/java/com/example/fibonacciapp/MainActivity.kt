package com.example.fibonacciapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlinx.coroutines.isActive
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var editText: EditText
    private lateinit var btn: Button
    private lateinit var textView: TextView

    private var isCalculating = false
    private var fibonacciIndex = 0
    private var fibonacciSequence = mutableListOf<Long>()
    private var calculationJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editText = findViewById(R.id.editText)
        btn = findViewById(R.id.btn)
        textView = findViewById(R.id.textView)

        btn.setOnClickListener {
            if (!isCalculating) {
                    lifecycleScope.launch { startCalculating()
                }
            } else {
                stopCalculating()
            }
        }
    }

    private fun startCalculating() {
        isCalculating = true
        btn.setText(R.string.cancel)
        editText.isEnabled = false

        val inputNumber = editText.text.toString().toIntOrNull() ?: 0

        if (inputNumber == 0) {
            Toast.makeText(this, "Please enter some Fibonacci number", Toast.LENGTH_SHORT).show()
            stopCalculating()
            return
        }

        calculationJob = CoroutineScope(Dispatchers.Main).launch {
            fibonacciSequence.clear()
            var current = 0L
            var next = 1L

            for (i in 0..inputNumber) {
                if (!isActive) {
                    break
                }
                fibonacciSequence.add(current)
                val temp = current
                current = next
                next += temp
                fibonacciIndex = i

                updateUI()

                try {
                    delay(1000)
                } catch (e: InterruptedException) {
                    coroutineContext.cancel()
                }
            }
            updateUI()
            stopCalculating()
        }
    }

    private fun stopCalculating() {
        calculationJob?.cancel()
        isCalculating = false
        btn.setText(R.string.start)
        editText.isEnabled = true
    }

    private fun updateUI() {
        if (fibonacciIndex >= 0 && fibonacciIndex < fibonacciSequence.size) {
            textView.text = String.format(Locale.getDefault(), "%d", fibonacciSequence[fibonacciIndex])
        }else {
            textView.setText(R.string.done)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(IS_CALCULATING_KEY, isCalculating)
        outState.putInt(FIBONACCI_INDEX_KEY, fibonacciIndex)
        outState.putLongArray(FIBONACCI_SEQUENCE_KEY, fibonacciSequence.toLongArray())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        isCalculating = savedInstanceState.getBoolean(IS_CALCULATING_KEY)
        fibonacciIndex = savedInstanceState.getInt(FIBONACCI_INDEX_KEY)
        fibonacciSequence = savedInstanceState.getLongArray(FIBONACCI_SEQUENCE_KEY)?.toMutableList() ?: mutableListOf()
        updateUI()
    }

    companion object {
        private const val IS_CALCULATING_KEY = "isCalculating"
        private const val FIBONACCI_INDEX_KEY = "fibonacciIndex"
        private const val FIBONACCI_SEQUENCE_KEY = "fibonacciSequence"
    }
}